FROM openjdk:11
RUN useradd -r myuser
WORKDIR /app
COPY target/assignment-*.jar /app/
ENV SPRING_PROFILES_ACTIVE=h2
ENV SERVER_PORT=8070
EXPOSE ${SERVER_PORT}
USER myuser
ENTRYPOINT ["sh", "-c", "java -jar -Dserver.port=${SERVER_PORT} -Dspring.profiles.active=${SPRING_PROFILES_ACTIVE} assignment-*.jar"]
