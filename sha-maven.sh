#!/bin/bash
DATE=$(git log -1 --date=format:%y%m%d --pretty=format:%ad HEAD)
HASH=$(git log -1 --pretty=format:%h HEAD)
VERSION="${DATE}-${HASH}"
VERSION_D="${DATE}-${HASH}"
echo "${VERSION}"
echo "${VERSION_D}"
mvn versions:set -DnewVersion=${VERSION}
